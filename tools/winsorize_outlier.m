function[y] = winsorize_outlier(x)
% WINSOR     Winsorize a vector 
% INPUTS   : x - n*1 data vector
% OUTPUTS  : y - winsorized x, n*1 vector

y = x;
i_outlier = find(isoutlier(x, 'median'));
i1 = i_outlier(x(i_outlier)<nanmedian(x));
i2 = i_outlier(x(i_outlier)>nanmedian(x));
if ~isempty(i1); y(i1) = min(x(setdiff(1:end,i1))); end
if ~isempty(i2); y(i2) = max(x(setdiff(1:end,i2))); end
