function [coords, region_names] = load_randy_regions(xlsfname)
  newData1 = importdata(xlsfname);
  
  coords = newData1.data.area_coords ;
  clear region_names;
  % skip first 3 lines - header
  for line = 4:size(newData1.textdata.AreaList,1)
   
    hemisphere = newData1.textdata.AreaList{line,7};
    regname = newData1.textdata.AreaList{line,6};
    region_names{line-3} = [hemisphere  ' ' regname];
  end
  
