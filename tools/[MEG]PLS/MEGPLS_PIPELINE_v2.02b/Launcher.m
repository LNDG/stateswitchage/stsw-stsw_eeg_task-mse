function varargout = Launcher(varargin)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Serves as the "launch hub" for the MEGPLS pipeline. %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Copyright (C) 2013-2014, Michael J. Cheung
%
% This file is a part of the MEG & PLS Pipeline (MEGPLS). For more 
% details, see the documentation included with the software package.
%
% MEGPLS is free software: you can redistribute it and/or modify it under
% the terms of the GNU General Public License version 2 as published by 
% the Free Software Foundation. This program is distributed in the hope 
% that it will be useful, but WITHOUT ANY WARRANTY; without even the 
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program. If not, you can download the license here: 
% <http://www.gnu.org/licenses/old-licenses/gpl-2.0>.


% Last Modified by GUIDE v2.5 17-Sep-2014 12:35:04

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Launcher_OpeningFcn, ...
                   'gui_OutputFcn',  @Launcher_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


%--- Executes just before Launcher is made visible. ---%
%------------------------------------------------------%
function Launcher_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Launcher (see VARARGIN)

% Choose default command line output for Launcher
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Launcher wait for user response (see UIRESUME)
% uiwait(handles.figure1);


%--- Outputs from this function are returned to the command line. ---%
%--------------------------------------------------------------------%
function varargout = Launcher_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;




% --- Executes on button press in ButtonSetPathsGUI.
function ButtonSetPathsGUI_Callback(hObject, eventdata, handles)
InstallationGUI


% --- Executes on button press in ButtonPreprocMEG.
function ButtonPreprocMEG_Callback(hObject, eventdata, handles)
PreprocMEG

% --- Executes on button press in ButtonPreprocMRI.
function ButtonPreprocMRI_Callback(hObject, eventdata, handles)
PreprocMRI

% --- Executes on button press in ButtonTrialCleaner.
function ButtonTrialCleaner_Callback(hObject, eventdata, handles)
TrialCleanerGUI

% --- Executes on button press in ButtonICAcleaner.
function ButtonICAcleaner_Callback(hObject, eventdata, handles)
ICAcleanerGUI


% --- Executes on button press in ButtonBuilderGUI.
function ButtonBuilderGUI_Callback(hObject, eventdata, handles)
BuilderGUI

% --- Executes on button press in ButtonDriverGUI.
function ButtonDriverGUI_Callback(hObject, eventdata, handles)
DriverGUI


% --- Executes on button press in ViewTimelockFreqData.
function ViewTimelockFreqData_Callback(hObject, eventdata, handles)
VisualizeTimeFreq([])

% --- Executes on button press in ButtonViewSourceData.
function ButtonViewSourceData_Callback(hObject, eventdata, handles)
VisualizeSourceData([])

% --- Executes on button press in ButtonViewPLSresults.
function ButtonViewPLSresults_Callback(hObject, eventdata, handles)
ViewPLSresults

% --- Executes on button press in ButtonViewManualNiftiAfni.
function ButtonViewManualNiftiAfni_Callback(hObject, eventdata, handles)
ViewNiftiAfni
