%================================================================%
% Logs error messages into ErrorLogs for pipeline:               %
% When multiple workers are open, "fprintf" within a parfor loop %
% will not write to files properly. Nest it in this function.    %
% Last modified: Sept. 24, 2014                                  %
%================================================================%

% This file is a part of the MEG & PLS Pipeline (MEGPLS). For more 
% details, see the documentation included with the software package.


function LogErrorMessage(ErrorMessage, RunSection)
LoadErrLog = fopen(['ErrorLog_',RunSection,'.txt'], 'a');

fprintf(LoadErrLog, ErrorMessage);

fclose(LoadErrLog);