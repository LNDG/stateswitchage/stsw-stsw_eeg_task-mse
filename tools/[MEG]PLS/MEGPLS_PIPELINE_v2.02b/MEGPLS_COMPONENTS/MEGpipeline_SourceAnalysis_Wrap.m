%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Runs Source Analysis & Localization in FieldTrip:                        %
% Inputs: Preprocessed MEG data, Source Filter, Headmodels, and Leadfield. %
% Last modified: Jan. 15, 2014                                             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Copyright (C) 2013-2014, Michael J. Cheung
%
% This file is a part of the MEG & PLS Pipeline (MEGPLS). For more 
% details, see the documentation included with the software package.
%
% MEGPLS is free software: you can redistribute it and/or modify it under
% the terms of the GNU General Public License version 2 as published by 
% the Free Software Foundation. This program is distributed in the hope 
% that it will be useful, but WITHOUT ANY WARRANTY; without even the 
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program. If not, you can download the license here: 
% <http://www.gnu.org/licenses/old-licenses/gpl-2.0>.


function MEGpipeline_SourceAnalysis_Wrap(BuilderMat)


% Make sure java paths added:
UseProgBar = CheckParforJavaPaths;


% Load builder .mat file:
Builder      = load(BuilderMat);
name         = Builder.name;
paths        = Builder.paths;
time         = Builder.time;
FTcfg        = Builder.FTcfg;
PipeSettings = Builder.PipeSettings;

SourceContrast     = PipeSettings.Source.Contrast;
EstNoiseCommonFilt = PipeSettings.Source.EstNoiseCommonFilt;


% Clear existing Errorlog & Diary:
if exist('ErrorLog_SourceAnalysis.txt', 'file')
    system('rm ErrorLog_SourceAnalysis.txt');
end
if exist('Diary_SourceAnalysis.txt', 'file')
    system('rm Diary_SourceAnalysis.txt');
end

diary Diary_SourceAnalysis.txt
ErrLog = fopen('ErrorLog_SourceAnalysis.txt', 'a');


% Check gradiometer info:
GradMismatch = CheckGradInfo(Builder, 'SourceAnalysis');

if GradMismatch == 1
    open('ErrorLog_SourceAnalysis.txt');
    error('ERROR: Custom-filter(s) cannot be applied due to gradiometer info. See ErrorLog.')
end



%==========================================%
% RUN SOURCE-ANALYSIS ON EACH TIME-WINDOW: %
%==========================================%

for g = 1:length(name.GroupID)
    NumSubj = length(name.SubjID{g});
    
    if UseProgBar == 1
        ppm = ParforProgMon...
            (['RUNNING SOURCE LOCALIZATION:  ',name.GroupID{g},'.  '], NumSubj, 1, 700, 80);
    end
    
    for s = 1:length(name.SubjID{g})
        
        
        %--- If DiffCond: Run source-analysis for control datasets: ---%
        %--------------------------------------------------------------%
        
        % Note: For each subject, grad info is the same between active & control datasets.
        % Therefore, we know all conditions & control datasets have the same common-filter.
        % We can use the same hdm, leadfield, ad filter for each subject.
        if strcmp(SourceContrast, 'DiffCond')
            clear MEGdataControl SourceControl Headmodel Leadfield Filter
            
            % Get paths to input data:
            MEGdataControl = paths.MEGdataControl{g}{s};
            Headmodel      = paths.Hdm{g}{s,1};
            Leadfield      = paths.Lead{g}{s,1};
            Filter         = paths.SourceFilter{g}{s,1};
            
            % Check paths here, so ErrorLog isn't flooded:
            CheckInput1 = CheckPipelineMat(MEGdataControl, 'SourceAnalysis');
            CheckInput2 = CheckPipelineMat(Headmodel,      'SourceAnalysis');
            CheckInput3 = CheckPipelineMat(Leadfield,      'SourceAnalysis');
            CheckInput4 = CheckPipelineMat(Filter,         'SourceAnalysis');
            
            if CheckInput1 == 0 || CheckInput2 == 0 || CheckInput3 == 0 || CheckInput4 == 0
                continue;
            end
            
            % Run source-analysis for each time-window:
            parfor t = 1:size(time.Windows, 1)
                TimeWindow = [time.Windows(t,1), time.Windows(t,2)];
                
                SourceControl{t} = MEGpipeline_SourceAnalysis_Guts...
                    (FTcfg, TimeWindow, Headmodel, Leadfield, Filter, MEGdataControl);
            end
            
            % Make sure all control sources computed:
            if max(cellfun(@isempty, SourceControl)) == 1  % Empty cells detected
                disp('ERROR: Failed to generate source data for control condition.')
                continue;
            end
        end
        
        % If not DiffCond, initialize SourceControl for parfor loop:
        if ~strcmp(SourceContrast, 'DiffCond')
            for t = 1:size(time.Windows, 1)
                SourceControl{t} = [];
            end
        end
        
        
        %--- Run source analysis for input datasets: ---%
        %-----------------------------------------------%
        
        for c = 1:length(name.CondID)
            clear MEGdata SourceBaseline Headmodel Leadfield Filter
            
            % Get paths to input data:
            MEGdata   = paths.MEGdata{g}{s,c};
            Headmodel = paths.Hdm{g}{s,c};
            Leadfield = paths.Lead{g}{s,c};
            Filter    = paths.SourceFilter{g}{s,c};
            
            % Check paths here, so ErrorLog isn't flooded:
            CheckInput1 = CheckPipelineMat(MEGdata,   'SourceAnalysis');
            CheckInput2 = CheckPipelineMat(Headmodel, 'SourceAnalysis');
            CheckInput3 = CheckPipelineMat(Leadfield, 'SourceAnalysis');
            
            if CheckInput1 == 0 || CheckInput2 == 0 || CheckInput3 == 0
                continue;
            end
            
            % Load & check filter:
            CheckFilter = LoadFTmat(Filter, 'SourceAnalysis');
            if isempty(CheckFilter)
                continue;
            end
            
            % Make sure method for filter calculation was the same:
            if ~isequal(CheckFilter.cfg.method, FTcfg.Source.method)
                fprintf(ErrLog, ['ERROR:'...
                    '\n Source-filter was computed using a different source method'...
                    '\n than the one currently specified for source analysis.'...
                    '\n Recompute filter using the current source method to continue.'...
                    '\n %s \n\n'], Filter);
                continue;
            end
            
            % Make sure lambda value for filter calculation was the same:
            CurrentLambda = [];
            FilterLambda  = [];
            
            if isfield(FTcfg.Source.(FTcfg.Source.method), 'lambda')
                CurrentLambda = FTcfg.Source.(FTcfg.Source.method).lambda;
            end
            if isfield(CheckFilter.cfg.(FTcfg.Source.method), 'lambda')
                FilterLambda  = CheckFilter.cfg.(FTcfg.Source.method).lambda;
            end
            
            if ~isequal(CurrentLambda, FilterLambda)
                fprintf(ErrLog, ['ERROR:'...
                    '\n Source-filter was computed using a different lambda value'...
                    '\n than the one currently specified for source analysis.'...
                    '\n Recompute filter using the current lambda value to continue.'...
                    '\n %s \n\n'], Filter);
                continue;
            end
            
            CheckFilter = [];  % Free memory
            
            
            % For DiffBase: Get power of baseline period for contrast:
            % Note: Using common-filter that will be used for active-period as well.
            if strcmp(SourceContrast, 'DiffBase')
                BaselineTime = PipeSettings.Source.DiffBaseTime;
                
                SourceBaseline = MEGpipeline_SourceAnalysis_Guts...
                    (FTcfg, BaselineTime, Headmodel, Leadfield, Filter, MEGdata);
                
                if isempty(SourceBaseline)
                    disp('ERROR: Failed to compute source data for baseline period.')
                    continue;
                end
            else
                SourceBaseline = [];  % Initialize for parfor later on
            end
            
            
            % Run source-analysis for each window:
            parfor t = 1:size(time.Windows, 1)
                SourceData = [];
                TimeWindow = [time.Windows(t,1), time.Windows(t,2)];
                
                SourceData = MEGpipeline_SourceAnalysis_Guts...
                    (FTcfg, TimeWindow, Headmodel, Leadfield, Filter, MEGdata);
                
                if isempty(SourceData)
                    disp('ERROR: Failed to compute source data for current time-window.')
                    continue;
                end
                
                
                % ** Remnant from older version. Keep commented for now...
                % Get noise from source-filter calculation: Ensures that the same
                % noise projection is used across MEG session. For common-filters,
                % we can assume the same noise bias across post & prestim or active & control.
                % ProjNoise = Filter.avg.noise;
                
                
                % Compute NAI (Neural Activity Index):
                ActivePower = SourceData.avg.pow;
                
                if strcmp(SourceContrast, 'DiffCond')
                    ControlPower = SourceControl{t}.avg.pow;
                elseif strcmp(SourceContrast, 'DiffBase')
                    ControlPower = SourceBaseline.avg.pow;
                end
                
                switch PipeSettings.Source.Output
                    case 'PseudoZ'
                        SourceData.avg.OutputStat = ActivePower ./ SourceData.avg.noise;
                        
                    case 'PseudoT'
                        SourceData.avg.OutputStat = ...
                            (ActivePower - ControlPower) ./ SourceData.avg.noise;
                        
                    case 'PseudoF'
                        SourceData.avg.OutputStat = ActivePower ./ ControlPower;
                end
                
                
                % Save final source results:
                CheckSavePath(paths.Source{g}{s,c,t}, 'SourceAnalysis');
                ParforSaveSource(paths.Source{g}{s,c,t}, SourceData)
                
                SourceData     = [];  % Free memory
                ActivePower    = [];
                %ControlPower   = [];
                
            end  % Time
            
        end  % Cond
        
        if UseProgBar == 1 && mod(s, 1) == 0
            ppm.increment();  % move up progress bar
        end
        
    end  % Subj
    if UseProgBar == 1
        ppm.delete();
    end
    
end  % Group



%=========================%
% CHECK FOR OUTPUT FILES: %
%=========================%

for g = 1:length(name.GroupID)
    for s = 1:length(name.SubjID{g})
        for c = 1:length(name.CondID)
            
            MissingFiles = 0;
            for t = 1:size(time.Windows, 1)
                [SourceFolder, SourceFile, Ext] = fileparts(paths.Source{g}{s,c,t});
                
                if ~exist(paths.Source{g}{s,c,t}, 'file')
                    if MissingFiles == 0
                        MissingFiles = 1;
                        
                        fprintf(ErrLog, ['\nERROR: Output source files missing for:'...
                            '\n %s \n'], SourceFolder);
                    end
                    
                    fprintf(ErrLog, ' - Time: %s \n', [SourceFile,Ext]);
                end
            end
            
        end  % Cond
    end  % Subj
end  % Group



%=================%

if exist([pwd,'/ErrorLog_SourceAnalysis.txt'], 'file')
    LogCheck = dir('ErrorLog_SourceAnalysis.txt');
    if LogCheck.bytes ~= 0  % File not empty
        open('ErrorLog_SourceAnalysis.txt');
    else
        delete('ErrorLog_SourceAnalysis.txt');
    end
end

fclose(ErrLog);
diary off

end



function ParforSaveSource(OutputPath, SourceData)
    SourceData
    save(OutputPath, 'SourceData');
end
