%==========================================%
% Check if required paths have been added. %
% Last modified: Jan.15, 2014              %
%==========================================%

% Copyright (C) 2013-2014, Michael J. Cheung
%
% This file is a part of the MEG & PLS Pipeline (MEGPLS). For more 
% details, see the documentation included with the software package.
%
% MEGPLS is free software: you can redistribute it and/or modify it under
% the terms of the GNU General Public License version 2 as published by 
% the Free Software Foundation. This program is distributed in the hope 
% that it will be useful, but WITHOUT ANY WARRANTY; without even the 
% implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
% See the GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License along
% with this program. If not, you can download the license here: 
% <http://www.gnu.org/licenses/old-licenses/gpl-2.0>.


function CheckToolboxPaths(PipelineRootDir)


% Check pipeline root directory:
if ~exist([PipelineRootDir,'/MEGPLS_COMPONENTS'], 'dir')
	message = {'Error:'; '';
		'Specified folder does not seem to be pipeline root directory.';
		'Cannot find MEGPLS_COMPONENTS folder in specified directory.'};
	
	msgbox(message, 'Error:')
	return;
end


% Check if pipeline paths added:
CheckPipe1 = which('InstallationGUI.m');
CheckPipe2 = which('MEGpipeline_RunPLS.m');

if isempty(CheckPipe1) || isempty(CheckPipe2)
	addpath(genpath(PipelineRootDir));
    rmpath([PipelineRootDir,'/DEFAULT_SETTINGS']);  % Only want to call the ones in AnalysisID
    rmpath([PipelineRootDir,'/TEMPORARY_FIXES']);   % Want to call from Fieldtrip toolbox
end


% Check if toolbox paths added:
CheckFT			= which('ft_defaults.m');
CheckSPM		= which('spm_get_defaults.m');
CheckAFNIMatlab = which('BrikLoad.m');

if isempty(CheckFT) || isempty(CheckSPM) || isempty(CheckAFNIMatlab)
	if exist([PipelineRootDir,'/ToolboxPaths.mat'], 'file')
		ToolboxPaths = load([PipelineRootDir,'/ToolboxPaths.mat']);
        
        %  Add Fieldtrip toolbox:
        if ~exist([ToolboxPaths.paths.Fieldtrip,'/ft_defaults.m'], 'file')
            ErrMsg = 'ERROR: Reselect Fieldtrip toolbox. Re-run InstallationGUI.';
            msgbox(ErrMsg);
            error(ErrMsg);
        else
            addpath(ToolboxPaths.paths.Fieldtrip);
            ft_defaults
        end
		
        % Add SPM8 toolbox:
        if ~exist([ToolboxPaths.paths.SPM,'/spm_get_defaults.m'], 'file')
            ErrMsg = 'ERROR: Reselect SPM8 toolbox. Re-run InstallationGUI.';
            msgbox(ErrMsg);
            error(ErrMsg);
        else
            addpath(ToolboxPaths.paths.SPM);
        end
        
        % Add AFNI-Matlab toolbox:
        if ~exist([ToolboxPaths.paths.AFNIMatlab,'/BrikLoad.m'], 'file')
            ErrMsg = 'ERROR: Reselect AFNI-Matlab toolbox. Re-run InstallationGUI.';
            msgbox(ErrMsg);
            error(ErrMsg);
        else
            addpath(ToolboxPaths.paths.AFNIMatlab);
        end
		
	else
		msgbox('ERROR: Toolbox paths not set. Run InstallationGUI.m first.')
		error('ToolboxPaths not set. Run InstallationGUI.m first.');
		
	end
end