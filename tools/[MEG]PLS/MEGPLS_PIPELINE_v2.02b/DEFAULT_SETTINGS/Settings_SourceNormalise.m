%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ADVANCED SETTINGS FOR SOURCE NORMALISATION: %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% This settings file will be read by the BuilderGUI.
% - Settings here will be loaded into the Builder .mat and applied to analysis.
%
% Usage:
%  a)  Settings under "DO NOT EDIT" are handled by [MEG]PLS. Do not alter these.
%  b)  Change other values as needed. See "help" command for FieldTrip or SPM sections.
%  c)  Optional settings can be uncommented and specified.
%
function [SPMcfg, cfgWriteNormMRI, cfgWriteNormSrc] = Settings_SourceNormalise



%--- "FT_VOLUMEWRITE" SETTINGS FOR NORMALISED MRI: ---%
%-----------------------------------------------------%

% Note: This section determines how normalised MRI files
%       are written out into NIFTI and AFNI format.

% DO NOT EDIT:
cfgWriteNormMRI.parameter  = 'anatomy';
cfgWriteNormMRI.coordsys   = 'spm';
cfgWriteNormMRI.filetype   = 'nifti';       % Currently hardcoded for NIFTI.
cfgWriteNormMRI.filename   = [];            % Handled by pipeline.

% GENERAL SETTINGS:
cfgWriteNormMRI.downsample = 1;            % Integer # to downsample (Default = 1; no downsampling).
cfgWriteNormMRI.datatype   = 'double';     % 'bit1', 'uint8', 'int16', 'int32', 'float' or 'double'
cfgWriteNormMRI.scaling    = 'no';         % Scale to max value of physical or statistical parameter.
%                                            Can change to Int16 & scale to avoid working in e-10 values.

% OPTIONAL SETTINGS:
%  cfgWriteNormMRI.fiducial.nas = [x y z] position of nasion
%  cfgWriteNormMRI.fiducial.lpa = [x y z] position of LPA
%  cfgWriteNormMRI.fiducial.rpa = [x y z] position of RPA
%  cfgWriteNormMRI.markfiducial = 'yes' or 'no', mark the fiducials
%  cfgWriteNormMRI.markorigin   = 'yes' or 'no', mark the origin
%  cfgWriteNormMRI.markcorner   = 'yes' or 'no', mark the first corner of the volume



%--- "FT_VOLUMEWRITE" SETTINGS FOR NORMALISED SOURCES: ---%
%---------------------------------------------------------%

% Note: This section determines how normalised source files
%       are written out into NIFTI and AFNI format.

% DO NOT EDIT:
cfgWriteNormSrc.filename   = [];          % Handled by pipeline.
cfgWriteNormSrc.filetype   = 'nifti';     % Currently hardcoded for NIFTI.

% GENERAL SETTINGS:
cfgWriteNormSrc.downsample = 1;
cfgWriteNormSrc.parameter  = 'avg.OutputStat';
cfgWriteNormSrc.datatype   = 'double';
cfgWriteNormSrc.scaling    = 'no';

% OPTIONAL SETTINGS:
%  cfgWriteNormSrc.fiducial.nas = [x y z] position of nasion
%  cfgWriteNormSrc.fiducial.lpa = [x y z] position of LPA
%  cfgWriteNormSrc.fiducial.rpa = [x y z] position of RPA
%  cfgWriteNormSrc.markfiducial = 'yes' or 'no', mark the fiducials
%  cfgWriteNormSrc.markorigin   = 'yes' or 'no', mark the origin
%  cfgWriteNormSrc.markcorner   = 'yes' or 'no', mark the first corner of the volume



%--- SPM SETTINGS FOR "SPM_NORMALISE" AND "SPM_WRITE_SN": ---%
%------------------------------------------------------------%

% DO NOT EDIT: Load default SPM8 settings.
Defaults  = spm_get_defaults;
Estimate  = Defaults.normalise.estimate;
Write     = Defaults.normalise.write;
Write.vox = [];                         % Handled by pipeline (Resolution of functional images used).

% SETTINGS FOR "SPM_NORMALISE":
Estimate.smoref  = 0;                   % Default SPM template is already smoothed by 8mm.
Estimate.smosrc  = 8;                   % Use 8 to match with default SPM smoothed template.
Estimate.regtype = 'mni';
Estimate.cutoff  = 25;
Estimate.nits    = 16;                  % Number of non-linear iterations (Set to 0 for linear-only).
Estimate.reg     = 1;

% SETTINGS FOR "SPM_WRITE_SN":
Write.prefix     = 'w';
Write.preserve   = 0;
Write.bb         = [-78 -112 -50; 78 76 85];
Write.interp     = 1;
Write.wrap       = [0 0 0];



%--- DO NOT EDIT: SAVE OUTPUT CONFIGS: ---%
%-----------------------------------------%

SPMcfg.Estimate  = Estimate;
SPMcfg.Write	 = Write;



