function [h,D] = levcook(x,y,p)
% File name: 'cookd.m'. Function for calculating Cook's distance
% and leverage without using the Statistic Toolbox.
% 
% Formula for leverage:
% h = 1/L + (x-mean(x)).^2./sum((x-mean(x)).^2)
% 
% Formula for Cook's distance:
% D = (err./(RMSE*p)).*(h./((1-h).^2))
% 
% Three input parameters:   'x', 'y' and 'p'
% Two output parameters:    'h' and 'D'
% 
% x:    Vector of x-variables
% y:    Vector of y-variables
% p:    Number of regression parameters (Default = 2)
% h:    Leverage
% D:    Cook's distance
% 
% Additionally, plots for leverage and Cook's distance will be 
% generated.
% 
% Developed by Joris Meurs BASc (2016)

% Limitations
if length(x) ~= length(y), error('Vectors do not have same size');end
if nargin > 3, error('Too many input arguments');end
if nargin < 3 || isempty(p), p = 2; end
if nargin < 2, error('Not enough input parameters');end

% Linear regression 
L = length(x);
X = [ones(length(x),1) x];
b = pinv(X'*X)*X'*y;
y_hat = X*b;

% Error
err = (y_hat-y).^2;

%RMSE
err_y = y-y_hat; 
RMSE = pinv(L-p)*err_y'*err_y;

% Leverage
mean_x = sum(x)/L;
h1 = (x-mean_x).^2;
h2 = sum((x-mean_x).^2);
h3 = 1/L;
h = (h1./h2)+h3;

% Cook's d
D = (err./(RMSE*p)).*(h./((1-h).^2));

% Output figures
figure;
plot(x,h,'s','MarkerFaceColor','Green',...
    'MarkerEdgeColor','Green');
xlabel('X-variable');
ylabel('Leverage');
title('Leverage plot');

figure;
plot(y,D,'s','MarkerFaceColor','Red',...
    'MarkerEdgeColor','Red');
xlabel('Y-variable');
ylabel('Cook''s distance');
title('Cook''s distance plot');

end
