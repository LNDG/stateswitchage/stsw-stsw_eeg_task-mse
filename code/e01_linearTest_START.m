clc; close all; clear all;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
pn.root = pwd;

addpath(fullfile(pn.root, 'code'));

e01_linearTest('ya')
e01_linearTest('oa')
e01_linearTest('yaoa')

disp('Step finished!')

close all; clc;