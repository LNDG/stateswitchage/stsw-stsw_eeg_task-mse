clc; close all; clear all;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
pn.root = pwd;

addpath(fullfile(pn.root, 'code'));

e02_plot_traces('ya')
e02_plot_traces('oa')
e02_plot_traces('yaoa')

disp('Step finished!')

close all; clc;