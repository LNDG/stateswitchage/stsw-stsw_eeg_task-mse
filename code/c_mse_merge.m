function mseavg = c_mse_merge()

% collect mMSE outputs in a joint group structure

%% set up paths

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
rootpath = pwd;

pn.dataInOUT  = fullfile(rootpath, 'data', 'mmse_output');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

filename = fullfile(rootpath, 'code', 'id_list.txt');
fileID = fopen(filename);
IDs = textscan(fileID,'%s');
fclose(fileID);
IDs = IDs{1};

cond_names  = {'dim1'; 'dim2'; 'dim3'; 'dim4'};

% load example mse to get nchan etc
fprintf('Loading IDsect %s Session %d: . . .\n', 1, 1)
runname = sprintf('%s_%s_MSE_OUT.mat', IDs{1}(1:4), cond_names{1});
load(fullfile(pn.dataInOUT, runname))

% JQK: dimord should be: 'chan_scales_time'
ntim = size(mse.sampen,3);
nchan = size(mse.sampen,1);
nscales = size(mse.sampen,2);

mseavg.dat = [];
mseavg.dat = NaN(numel(IDs),nchan,nscales, ntim, 4); % IDs sess channel scale time cond
mseavg.r = NaN(numel(IDs),nchan,nscales, ntim, 4);
mseavg.time = mse.time;
mseavg.scale = mse.timescales;
mseavg.freq = mse.cfg.freq;

% collect mse results across IDsects etc.
for indID = 1:numel(IDs)
    for indCond = 1:4
        fprintf('Loading IDsect %s Condition %s: . . .\n', IDs{indID}(1:4), cond_names{indCond})
        runname = sprintf('%s_%s_MSE_OUT.mat', IDs{indID}(1:4), cond_names{indCond});
        load(fullfile(pn.dataInOUT, runname))
        mseavg.dat(indID,:,:,:,indCond) = mse.sampen;
        mseavg.r(indID,:,:,:,indCond) = mse.r;
    end
end

mseavg.IDs = IDs;
mseavg.mse_leg = {'MSEn'};
mseavg.dimord = 'subj_sess_chan_scale_time_cond';
mseavg.dimordsize = size(mseavg.dat);

%% save

save(fullfile(pn.dataInOUT, 'mseavg.mat'), 'mseavg');
