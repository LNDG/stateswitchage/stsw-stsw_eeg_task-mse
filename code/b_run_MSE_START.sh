#!/bin/bash

fun_name="b_run_MSE"
job_name="stsw_mMSE_gpu"

rootpath="$(pwd)/.."
rootpath=$(builtin cd $rootpath; pwd)

mkdir ${rootpath}/log

# path to the text file with all subject ids:
path_ids="${rootpath}/code/id_list.txt"
# read subject ids from the list of the text file
IDS=$(cat ${path_ids} | tr '\n' ' ')

Conds="dim1 dim2 dim3 dim4"

for subj in $IDS; do 
for cond in $Conds; do 
	echo ${job_name}_${subj}_${cond}
	if [ ! -f ${rootpath}/data/mmse_output/${subj}_${cond}_MSE_OUT.mat ]; then
  	sbatch \
  		--job-name ${job_name}_${subj}_${cond} \
  		--cpus-per-task 1 \
  		--gres gpu:pascal:1 \
		--partition gpu \
  		--mem 6gb \
  		--time 02:00:00 \
  		--output ${rootpath}/log/${job_name}_${subj}_${cond}.out \
  		--workdir . \
  		--wrap=". /etc/profile ; module load matlab/R2016b; matlab -nodisplay -r \"${fun_name}('${rootpath}','${subj}','${cond}')\""
	fi
done
done