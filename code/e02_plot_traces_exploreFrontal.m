function e02_plot_traces%(group)
group = 'yaoa'
% plot sample entropy traces

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
rootpath = pwd;

pn.data         = fullfile(rootpath, 'data');
pn.dataInOUT    = fullfile(rootpath, 'data', 'mmse_output');
pn.figures      = fullfile(rootpath, 'figures');
pn.tools        = fullfile(rootpath, 'tools');
    addpath(genpath(fullfile(pn.tools, 'barwitherr')))
    addpath(genpath(fullfile(pn.tools, 'shadedErrorBar')))
    addpath(genpath(fullfile(pn.tools, 'winsor')))
    addpath(genpath(fullfile(pn.tools, 'Cookdist')))

%% load data

load(fullfile(pn.dataInOUT, 'mseavg.mat'), 'mseavg');

ageIdx{1} = cellfun(@str2num, mseavg.IDs, 'un', 1)<2000;
ageIdx{2} = cellfun(@str2num, mseavg.IDs, 'un', 1)>2000;

% restrict to age group
if strcmp(group, 'ya')
    mseavg.r = mseavg.r(ageIdx{1},:,:,:,:);
    mseavg.dat = mseavg.dat(ageIdx{1},:,:,:,:);
    mseavg.IDs = mseavg.IDs(ageIdx{1});
elseif strcmp(group, 'oa')
    mseavg.r = mseavg.r(ageIdx{2},:,:,:,:);
    mseavg.dat = mseavg.dat(ageIdx{2},:,:,:,:);
    mseavg.IDs = mseavg.IDs(ageIdx{2});
elseif strcmp(group, 'yaoa')
    mseavg.r = mseavg.r(ageIdx{2},:,:,:,:);
    mseavg.dat = mseavg.dat(:,:,:,:,:);
    mseavg.IDs = mseavg.IDs(:);
end

mseavg.datBL=mseavg.dat;

% BLlims = [2 2.75]; % within-condition mean baseline
% mseavg.datBL=mseavg.dat-repmat(nanmean(mseavg.dat(:,:,:,mseavg.time>=BLlims(1) & mseavg.time<BLlims(2),:),4), 1,1,1,numel(mseavg.time),1);

%% plot temporal traces

%statsChans = find(squeeze(nanmean(nanmean(stat.mask(:,1,stat.time>3.5 & stat.time<6),3),3),2))>0);

statsChans = 10:12;
scale = 1:18;
% shading presents within-subject standard errors
% new value = old value ??? subject average + grand average
    
    h = figure('units','normalized','position',[.1 .1 .16 .18]); cla; hold on;
    
%     % highlight different experimental phases in background
%     patches.timeVec = [3 6]-3;
%     patches.colorVec = [1 .95 .8];
%     for indP = 1:size(patches.timeVec,2)-1
%         YLim = [-.25 1];
%         p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
%                     [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
%         p.EdgeColor = 'none';
%     end
    
    %mseavg.datBL(15,:,:,:,:) = NaN;

    condAvg = squeeze(nanmean(nanmean(nanmean(mseavg.datBL(:,statsChans,scale,:,1:4),3),2),5));
    curData = squeeze(nanmean(nanmean(mseavg.datBL(:,statsChans,scale,:,1),3),2));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(mseavg.time-3,nanmean(curData,1),standError, ...
        'lineprops', {'color', 6.*[.15 .1 .1],'linewidth', 2}, 'patchSaturation', .25);
    curData = squeeze(nanmean(nanmean(mseavg.datBL(:,statsChans,scale,:,2),3),2));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(mseavg.time-3,nanmean(curData,1),standError, ...
        'lineprops', {'color', 5.*[.2 .1 .1],'linewidth', 2}, 'patchSaturation', .25);
    curData = squeeze(nanmean(nanmean(mseavg.datBL(:,statsChans,scale,:,3),3),2));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(mseavg.time-3,nanmean(curData,1),standError, ...
        'lineprops', {'color', 3.*[.3 .1 .1],'linewidth', 2}, 'patchSaturation', .25);
    curData = squeeze(nanmean(nanmean(mseavg.datBL(:,statsChans,scale,:,4),3),2));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(mseavg.time-3,nanmean(curData,1),standError, ...
        'lineprops', {'color', 2.*[.3 .1 .1],'linewidth', 2}, 'patchSaturation', .25);

    xlim([-1 4]);
    minmax = [min(nanmean(condAvg,1)), max(nanmean(condAvg,1))];
    ylim(minmax+[-0.2*diff(minmax), 0.2*diff(minmax)])
    
    xlabel('Time (s from stimulus onset)'); ylabel([{'Sample entropy'}]);
    leg = legend([l1.mainLine, l2.mainLine, l3.mainLine, l4.mainLine], {'1 Target', '2 Targets','3 Targets', '4 Targets'},...
        'orientation', 'vertical', 'location', 'South'); legend('boxoff')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    set(leg,'FontSize',15)
% figureName = ['e02_EntropyTraces_',group];
% saveas(h, fullfile(pn.figures, figureName), 'epsc');
% saveas(h, fullfile(pn.figures, figureName), 'png');