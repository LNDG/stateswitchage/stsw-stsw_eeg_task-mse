% Probe parametric effect

clear all; cla; clc; restoredefaultpath;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..', '..'))
rootpath = pwd;

pn.data         = fullfile(rootpath, 'data');
pn.dataInOUT    = fullfile(pn.data, 'C_MSE_Output_v2');
pn.figures      = fullfile(rootpath, 'figures');
pn.tools        = fullfile(rootpath, 'tools');
    addpath(pn.tools) % requires mysigstar_vert!
    addpath(genpath(fullfile(pn.tools, '[MEG]PLS', 'MEGPLS_PIPELINE_v2.02b')))
    addpath(fullfile(pn.tools, 'fieldtrip')); ft_defaults;
    addpath(genpath(fullfile(pn.tools, 'barwitherr')))
    addpath(genpath(fullfile(pn.tools, 'RainCloudPlots')))
    addpath(genpath(fullfile(pn.tools, 'BrewerMap')))
    addpath(genpath(fullfile(pn.tools, 'shadedErrorBar')))

% set custom colormap
cBrew = brewermap(500,'RdBu');
cBrew = flipud(cBrew);

%% load data

load(fullfile(pn.dataInOUT, 'mseavg_CSD_YA.mat'), 'mseavg');
load(fullfile(pn.data, 'elec.mat'));

% mseavg.datBL=mseavg.dat;
% mseavg.rBL=mseavg.r;

BLlims = [2.5 3];
mseavg.datBL=10*log10(mseavg.dat./repmat(nanmean(nanmean(mseavg.dat(:,:,:,find(mseavg.time>=BLlims(1) & mseavg.time<BLlims(2)),:),5),4), 1,1,1,numel(mseavg.time),4));
mseavg.rBL=10*log10(mseavg.r./repmat(nanmean(nanmean(mseavg.r(:,:,:,find(mseavg.time>=BLlims(1) & mseavg.time<BLlims(2)),:),5),4), 1,1,1,numel(mseavg.time),4));

%% estimate linear effects (i.e. first level-within subject)
    
b = []; b_r = [];
for indID = 1:size(mseavg.datBL,1)
    for indChan = 1:60
        for indScale = 1:2%:numel(mseavg.scale)
            for indTime = 1:numel(mseavg.time)
                X = [1,1;1,2;1,3;1,4];
                y = [squeeze(mseavg.datBL(indID,indChan,indScale,indTime,:))];
                y2 = [squeeze(mseavg.rBL(indID,indChan,indScale,indTime,:))];
                b(indID,indChan,indScale,indTime,:) = regress(y,X);
                b_r(indID,indChan,indScale,indTime,:) = regress(y2,X);
            end
        end
    end
end

%% regression with linear drift changes

pn.summary	= fullfile(rootpath, '..', '..', 'stsw_multimodal', 'data');

load(fullfile(pn.summary, 'STSWD_summary_YAOA.mat'))

idx_summary = find(ismember(STSWD_summary.IDs, mseavg.SUBJ));
idx_MSE = find(ismember(mseavg.SUBJ,STSWD_summary.IDs));

StatStruct = [];
for indCond = 1
    StatStruct{indCond}.label    = elec.label;
    StatStruct{indCond}.freq     = mseavg.scale; % freq actually represents the scale here
    StatStruct{indCond}.time     = mseavg.time;
    StatStruct{indCond}.msedatadimord   = 'subj_chan_freq_time';
    StatStruct{indCond}.msedata  = squeeze(b(idx_MSE,:,:,:,2));
    StatStruct{indCond}.elec     = elec;
end

% prepare_neighbours determines what sensors may form clusters
cfg_neighb.method       = 'template';
cfg_neighb.template     = 'elec1010_neighb.mat';
cfg_neighb.channel      = elec.label;

cfgStat = [];
cfgStat.channel          = 'all';
cfgStat.frequency        = [1];
cfgStat.latency          = [1 7];
cfgStat.avgovertime      = 'no';
cfgStat.method           = 'montecarlo';
cfgStat.statistic        = 'ft_statfun_indepsamplesregrT';
cfgStat.correctm         = 'cluster';
cfgStat.clusteralpha     = 0.05;
cfgStat.clusterstatistic = 'maxsum';
%cfgStat.minnbchan        = 2;
cfgStat.tail             = 0;
cfgStat.clustertail      = 0;
cfgStat.alpha            = 0.025;
cfgStat.numrandomization = 1000;
cfgStat.parameter        = 'msedata';
cfgStat.neighbours       = ft_prepare_neighbours(cfg_neighb, StatStruct{1});

subj = size(StatStruct{1}.msedata,1);
conds = 1;
design = zeros(2,conds*subj);
for indCond = 1%:conds
for i = 1:subj
    design(1,(indCond-1)*subj+i) = STSWD_summary.HDDM_vat.driftEEG(idx_summary(i),1);
end
end
cfgStat.design   = design;
cfgStat.ivar     = 1;

[stat] = ft_freqstatistics(cfgStat,StatStruct{1});

% figure; imagesc(squeeze(stat.stat.*stat.mask))
% figure; imagesc(stat.time,[],squeeze(nanmean(stat.stat(:,1,:),2)))
% figure; imagesc(stat.time,[],squeeze(nanmean(stat.stat(:,10,:),2)))

save(fullfile(pn.data, 'E_statistics','L1_drift_regression_MSE.mat'), 'stat', 'cfgStat');

%% plot parietal topography

load(fullfile(pn.data, 'E_statistics','L1_drift_regression_MSE.mat'), 'stat', 'cfgStat');

cfg = [];
cfg.layout = 'acticap-64ch-standard2.mat';
cfg.parameter = 'powspctrm';
cfg.comment = 'no';
cfg.style = 'both';
cfg.colormap = cBrew;
cfg.zlim = [-4 4];
cfg.highlight = 'on';
statsChans = find(squeeze(nanmean(nanmean(stat.mask(:,1,stat.time>3.5 & stat.time<6),3),2))>0);
cfg.highlightchannel = stat.label(statsChans);

h = figure('units','normalized','position',[.1 .1 .7 .3]);
subplot(1,2,1);
    cfg.figure = h;
    plotData = [];
    plotData.label = stat.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(nanmean(nanmean(stat.stat(:,1,stat.time>3.5 & stat.time<6),3),2));
    %plotData.powspctrm = squeeze(nanmean(nanmean(stat.stat(:,10,stat.time>3.5 & stat.time<6),3),2));
    ft_topoplotER(cfg,plotData);
%     pval = []; pval = convertPtoExponential(stat.posclusters(1).prob);
%     title({'Interindividual differences in AMF relate to';['linear effects in parietal entropy: p = ', pval{1}]})
% plot temporal designation
subplot(1,2,2); cla;
    cfg.figure = h;
    imagesc(stat.time,[],squeeze(nanmean(stat.stat(:,1,:),2)), 'AlphaData', .2); 
    %caxis([-6 6]) 
    hold on;
    imagesc(stat.time,[],squeeze(nanmean(stat.stat(:,1,:),2)), 'AlphaData', squeeze(nanmean(stat.mask(:,1,:),2))); 
    xlabel('Time (s)'); ylabel('Channel');
    cb = colorbar; set(get(cb,'label'),'string','t values');
    line([3,3], [0,60], 'LineStyle', '--', 'Color', 'k', 'LineWidth', 2)
    line([6,6], [0,60], 'LineStyle', '--', 'Color', 'k', 'LineWidth', 2)
    title('Change in entropy during visual processing')
set(findall(gcf,'-property','FontSize'),'FontSize',18)
% pn.plotFolder = [pn.root, 'C_figures/'];
% figureName = 'L1_AMF_linearEntropy';
% 
% saveas(h, [pn.plotFolder, figureName], 'fig');
% saveas(h, [pn.plotFolder, figureName], 'epsc');
% saveas(h, [pn.plotFolder, figureName], 'png');

%% assess linearity in cluster

y = [squeeze(nanmean(nanmean(mseavg.dat(:,statsChans,1,mseavg.time>3.5 & mseavg.time<6,:),4),2))];

figure; bar(squeeze(nanmean(y,1)));
[h, p] = ttest(y(:,1),y(:,2))
[h, p] = ttest(y(:,2),y(:,3))
[h, p] = ttest(y(:,3),y(:,4))
