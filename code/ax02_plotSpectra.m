% Plot 1/f spectra from MSE inputs

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
rootpath = pwd;

pn.data = fullfile(rootpath, 'data', 'mmse_freq');
pn.figures = fullfile(rootpath, 'figures');

filename = fullfile(rootpath, 'code', 'id_list.txt');
fileID = fopen(filename);
IDs = textscan(fileID,'%s');
fclose(fileID);
IDs = IDs{1};

spectrum_avg = [];
for indID = 1:numel(IDs)
    id = IDs{indID};
    load(fullfile(pn.dataOut, [id, '_freq.mat']), 'freq');
    spectrum_avg(indID,:,:) = squeeze(nanmean(freq.powspctrm,1));
end

h = figure('units','centimeters','position',[0 0 10 7]);
set(gcf,'renderer','Painters')
plot(log10(freq.freq), squeeze(nanmean(nanmean(log10(spectrum_avg(:,58:60,:)),2),1)), ...
    'k', 'LineWidth', 2)
ylim([-10 -7.7])
xlabel('Frequency (log10Hz)'); ylabel('Power (log10)'); title('mMSE input spectrum')
set(findall(gcf,'-property','FontSize'),'FontSize',16)

figureName = ['a3_input_spectrum'];
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');
