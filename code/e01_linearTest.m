function e01_linearTest(group)

% Probe parametric effect

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
rootpath = pwd;

pn.data         = fullfile(rootpath, 'data');
pn.dataInOUT    = fullfile(rootpath, 'data', 'mmse_output');
pn.figures      = fullfile(rootpath, 'figures');
pn.tools        = fullfile(rootpath, 'tools');
    addpath(pn.tools) % requires mysigstar_vert!
    addpath(genpath(fullfile(pn.tools, '[MEG]PLS', 'MEGPLS_PIPELINE_v2.02b')))
    addpath(fullfile(pn.tools, 'fieldtrip')); ft_defaults;
    addpath(genpath(fullfile(pn.tools, 'barwitherr')))
    addpath(genpath(fullfile(pn.tools, 'RainCloudPlots')))
    addpath(genpath(fullfile(pn.tools, 'BrewerMap')))
    addpath(genpath(fullfile(pn.tools, 'shadedErrorBar')))
    addpath(genpath(fullfile(pn.tools, 'winsor')))
    addpath(genpath(fullfile(pn.tools, 'Cookdist')))
pn.summary	= fullfile(rootpath, '..', '..', 'stsw_multimodal', 'data');

% set custom colormap
cBrew = brewermap(500,'RdBu');
cBrew = flipud(cBrew);

%% load data

load(fullfile(pn.dataInOUT, 'mseavg.mat'), 'mseavg');

ageIdx{1} = cellfun(@str2num, mseavg.IDs, 'un', 1)<2000;
ageIdx{2} = cellfun(@str2num, mseavg.IDs, 'un', 1)>2000;

% restrict to age group
if strcmp(group, 'ya')
    mseavg.r = mseavg.r(ageIdx{1},:,:,:,:);
    mseavg.dat = mseavg.dat(ageIdx{1},:,:,:,:);
    mseavg.IDs = mseavg.IDs(ageIdx{1});
elseif strcmp(group, 'oa')
    mseavg.r = mseavg.r(ageIdx{2},:,:,:,:);
    mseavg.dat = mseavg.dat(ageIdx{2},:,:,:,:);
    mseavg.IDs = mseavg.IDs(ageIdx{2});
elseif strcmp(group, 'yaoa')
    mseavg.r = mseavg.r(ageIdx{2},:,:,:,:);
    mseavg.dat = mseavg.dat(:,:,:,:,:);
    mseavg.IDs = mseavg.IDs(:);
end

mseavg.datBL=mseavg.dat;
mseavg.rBL=mseavg.r;

%% create matrix for input to statistics

load(fullfile(pn.data, 'elec.mat'));

msestruct_dim = [];
for indCond = 1:4
    msestruct_dim{indCond}.label    = elec.label;
    msestruct_dim{indCond}.freq     = mseavg.scale; % freq actually represents the scale here
    msestruct_dim{indCond}.time     = mseavg.time;
    msestruct_dim{indCond}.msedatadimord   = 'subj_chan_freq_time';
    msestruct_dim{indCond}.msedata  = squeeze(mseavg.datBL(:,:,:,:,indCond));
    msestruct_dim{indCond}.elec     = elec;
end

%% parametric test at second level: 1st scale

% prepare_neighbours determines what sensors may form clusters
cfg_neighb.method       = 'template';
cfg_neighb.template     = 'elec1010_neighb.mat';
cfg_neighb.channel      = elec.label;

cfgStat = [];
cfgStat.channel          = 'all';
cfgStat.frequency        = [1];
cfgStat.latency          = [2 8];
cfgStat.avgovertime      = 'no';
cfgStat.method           = 'montecarlo';
cfgStat.statistic        = 'ft_statfun_depsamplesregrT';
cfgStat.correctm         = 'cluster';
cfgStat.clusteralpha     = 0.05;
cfgStat.clusterstatistic = 'maxsum';
cfgStat.minnbchan        = 2;
cfgStat.tail             = 0;
cfgStat.clustertail      = 0;
cfgStat.alpha            = 0.025;
cfgStat.numrandomization = 1000;
cfgStat.parameter        = 'msedata';
cfgStat.neighbours       = ft_prepare_neighbours(cfg_neighb, msestruct_dim{1});

subj = size(msestruct_dim{1}.msedata,1);
conds = 4;
design = zeros(2,conds*subj);
for indCond = 1:conds
for i = 1:subj
    design(1,(indCond-1)*subj+i) = indCond;
    design(2,(indCond-1)*subj+i) = i;
end
end
cfgStat.design   = design;
cfgStat.ivar     = 1;
cfgStat.uvar     = 2;

[stat] = ft_freqstatistics(cfgStat, msestruct_dim{1}, msestruct_dim{2}, msestruct_dim{3}, msestruct_dim{4});

save(fullfile(pn.data, 'E_statistics',['e01_',group,'.mat']), 'stat', 'cfgStat');

% figure; imagesc(squeeze(stat.posclusterslabelmat.*stat.stat))
% figure; imagesc(squeeze(stat.negclusterslabelsmat.*stat.stat))
% figure; imagesc(squeeze(stat.mask))

%% load statistics

load(fullfile(pn.data, 'E_statistics',['e01_',group,'.mat']), 'stat', 'cfgStat');

%% visualize topography and loading structure of load effect

cfg = [];
cfg.layout = 'acticap-64ch-standard2.mat';
cfg.parameter = 'powspctrm';
cfg.comment = 'no';
cfg.style = 'both';
cfg.colormap = cBrew;
cfg.colorbar = 'EastOutside';
cfg.zlim = [-3 3];
cfg.highlight = 'on';
statsChans = find(squeeze(nanmean(nanmean(stat.mask(:,1,stat.time>3.5 & stat.time<6),3),2))>0);
cfg.marker = 'off';  
cfg.highlight = 'yes';
cfg.highlightchannel = stat.label(statsChans);
cfg.highlightcolor = [0 0 0];
cfg.highlightsymbol = '.';
cfg.highlightsize = 40;

h = figure('units','normalized','position',[.1 .1 .3 .2]);
subplot(1,2,1);
    cfg.figure = h;    
    plotData = [];
    plotData.label = stat.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(nanmean(nanmean(stat.stat(:,1,stat.time>3.5 & stat.time<6),3),2));
    ft_topoplotER(cfg,plotData);
% plot temporal designation
subplot(1,2,2); cla;
    imagesc(stat.time,[],squeeze(nanmean(stat.stat(:,1,:),2)), 'AlphaData', .2); 
    caxis([-5 5]) 
    hold on;
    imagesc(stat.time,[],squeeze(nanmean(stat.stat(:,1,:),2)), 'AlphaData', squeeze(nanmean(stat.mask(:,1,:),2))); 
    xlabel('Time (s)'); ylabel('Channel');
    cb = colorbar; set(get(cb,'label'),'string','t values');
    line([3,3], [0,60], 'LineStyle', '--', 'Color', 'k', 'LineWidth', 2)
    line([6,6], [0,60], 'LineStyle', '--', 'Color', 'k', 'LineWidth', 2)
set(findall(gcf,'-property','FontSize'),'FontSize',18)
figureName = ['e01_EntropyTopo_',group];
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

%% plot temporal traces

%statsChans = find(squeeze(nanmean(nanmean(stat.mask(:,1,stat.time>3.5 & stat.time<6),3),2))>0);

statsChans = 58:60;

% shading presents within-subject standard errors
% new value = old value ??? subject average + grand average
    
    h = figure('units','normalized','position',[.1 .1 .16 .18]); cla; hold on;
    
    % highlight different experimental phases in background
    patches.timeVec = [3 6]-3;
    patches.colorVec = [1 .95 .8];
    for indP = 1:size(patches.timeVec,2)-1
        YLim = [-.25 1];
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end
    
    condAvg = squeeze(nanmean(nanmean(mseavg.datBL(:,statsChans,1,:,1:4),2),5));
    curData = squeeze(nanmean(mseavg.datBL(:,statsChans,1,:,1),2));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(mseavg.time-3,nanmean(curData,1),standError, ...
        'lineprops', {'color', 6.*[.15 .1 .1],'linewidth', 2}, 'patchSaturation', .25);
    curData = squeeze(nanmean(mseavg.datBL(:,statsChans,1,:,2),2));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(mseavg.time-3,nanmean(curData,1),standError, ...
        'lineprops', {'color', 5.*[.2 .1 .1],'linewidth', 2}, 'patchSaturation', .25);
    curData = squeeze(nanmean(mseavg.datBL(:,statsChans,1,:,3),2));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(mseavg.time-3,nanmean(curData,1),standError, ...
        'lineprops', {'color', 3.*[.3 .1 .1],'linewidth', 2}, 'patchSaturation', .25);
    curData = squeeze(nanmean(mseavg.datBL(:,statsChans,1,:,4),2));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(mseavg.time-3,nanmean(curData,1),standError, ...
        'lineprops', {'color', 2.*[.3 .1 .1],'linewidth', 2}, 'patchSaturation', .25);

    xlim([-1 4]);
    ylim([min(mean(condAvg,1))-.05*min(mean(condAvg,1)), ...
        max(mean(condAvg,1))+.05*max(mean(condAvg,1))])
    
    sigVals = double(squeeze(nanmean(stat.mask,1)));
    sigVals(sigVals==0) = NaN;
    sigVals(sigVals>0) = 1;
    hold on; plot(stat.time-3, sigVals.*min(mean(condAvg,1)), 'k', 'linewidth', 5)
    
    xlabel('Time (s from stimulus onset)'); ylabel([{'Sample entropy'}]);
    leg = legend([l1.mainLine, l2.mainLine, l3.mainLine, l4.mainLine], {'1 Target', '2 Targets','3 Targets', '4 Targets'},...
        'orientation', 'vertical', 'location', 'South'); legend('boxoff')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    set(leg,'FontSize',15)
figureName = ['e01_EntropyTraces_',group];
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

%% extract values within cluster
% calculate slopes first for each data point, then average (i.e. first level-within subject

individualMSE = [];
individualMSE.data = squeeze(nanmean(nanmean(mseavg.datBL(:,statsChans,1,mseavg.time>3.5 & mseavg.time<6,1:4),4),2));
X = [1 1; 1 2; 1 3; 1 4];
b=X\individualMSE.data';
individualMSE.data_slope(:,1) = b(2,:);
individualMSE.data_slope_win = winsor(individualMSE.data_slope, [0 95]);
individualMSE.IDs = mseavg.IDs;

save(fullfile(pn.data, 'E_statistics',['eO1_mse_',group,'.mat']), 'individualMSE');

%% raincloudplot of linear modulation

dataToPlot = individualMSE.data;

% define outlier as lin. modulation of 2.5*mean Cook's distance
cooks = Cookdist(dataToPlot(:,:));
outliers = cooks>2.5*mean(cooks);
idx_outlier = find(outliers);
idx_standard = find(outliers==0);

% read into cell array of the appropriate dimensions
data = []; data_ws = [];
for i = 1:4
    for j = 1:1
        data{i, j} = dataToPlot(:,i);
        % individually demean for within-subject visualization
        data_ws{i, j} = dataToPlot(:,i)-...
            nanmean(dataToPlot(:,:),2)+...
            repmat(nanmean(nanmean(dataToPlot(:,:),2),1),size(dataToPlot(:,:),1),1);
        data_nooutlier{i, j} = data{i, j};
        data_nooutlier{i, j}(idx_outlier) = [];
        data_ws_nooutlier{i, j} = data_ws{i, j};
        data_ws_nooutlier{i, j}(idx_outlier) = [];
        % sort outliers to back in original data for improved plot overlap
        data_ws{i, j} = [data_ws{i, j}(idx_standard); data_ws{i, j}(idx_outlier)];
    end
end

% IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

cl = [.6 .2 .2];

h = figure('units','normalized','position',[.1 .1 .2 .3]);
set(gcf,'renderer','Painters')
    cla;
    rm_raincloud_fixedSpacing(data_ws, [.8 .8 .8],1,[],[],[],15);
    h_rc = rm_raincloud_fixedSpacing(data_ws_nooutlier, cl,1,[],[],[],15);
    view([90 -90]);
    axis ij
box(gca,'off')
%set(gca, 'YTick', [1,2,3,4]);
set(gca, 'YTickLabels', {'4'; '3'; '2'; '1'});
ylabel('Target load'); xlabel({'Entropy'; '[Individually centered]'})
set(findall(gcf,'-property','FontSize'),'FontSize',30)
xlim([min(min(cat(2,data_ws{:})))-.05*min(min(cat(2,data_ws{:}))), ...
    max(max(cat(2,data_ws{:})))+.05*max(max(cat(2,data_ws{:})))])
    
% test linear effect
curData = [data_nooutlier{1, 1}, data_nooutlier{2, 1}, data_nooutlier{3, 1}, data_nooutlier{4, 1}];
X = [1 1; 1 2; 1 3; 1 4]; b=X\curData'; IndividualSlopes = b(2,:);
[~, p, ci, stats] = ttest(IndividualSlopes);
title({'Entropy modulation'; ['linear effect: p = ', num2str(round(p,3))]}); 

figureName = ['e01_entropy_modulation_RCplot_', group];
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');
