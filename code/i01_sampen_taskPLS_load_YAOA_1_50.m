% Set up EEG PLS for a task PLS using sample entropy

clear all; cla; clc;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
rootpath = pwd;

pn.data         = fullfile(rootpath, 'data');
pn.dataInOUT    = fullfile(pn.data, 'mmse_output');
pn.tools        = fullfile(rootpath, 'tools');
    addpath(genpath(fullfile(pn.tools, '[MEG]PLS', 'MEGPLS_PIPELINE_v2.02b')))
    addpath(fullfile(pn.tools, 'fieldtrip')); ft_defaults;
pn.slopes       = fullfile(rootpath, '..', 'aperiodic', 'data');
pn.fooof        = fullfile(rootpath, '..', 'aperiodic_fooof', 'data');
pn.summary	= fullfile(rootpath, '..', '..', 'stsw_multimodal', 'data');

%% add seed for reproducibility

rng(0, 'twister');

%% load data

filename = fullfile(rootpath, 'code', 'id_list.txt');
fileID = fopen(filename);
IDs = textscan(fileID,'%s');
fclose(fileID);
IDs = IDs{1};

ageIdx{1} = cellfun(@str2num, IDs, 'un', 1)<2000;
ageIdx{2} = cellfun(@str2num, IDs, 'un', 1)>2000;

load(fullfile(pn.dataInOUT, 'mseavg.mat'), 'mseavg');

% restrict to age group
for indGroup = 1:2
    TFRdata{indGroup}.mseavg = mseavg;
    TFRdata{indGroup}.mseavg.dat = mseavg.dat(ageIdx{indGroup},:,:,:,:);
    TFRdata{indGroup}.mseavg.IDs = mseavg.IDs(ageIdx{indGroup});
end

%mseavg.dat % sub*chan*scale*time*cond

% load example data
tmp_data = load(fullfile(pn.data, 'mmse_input','1117_dim1_MSE_IN.mat'));

time = TFRdata{1}.mseavg.time;
freq = TFRdata{1}.mseavg.freq;
elec = tmp_data.data.elec;

% create datamat

idx_chan = 1:50;
timeIdx = find(time > 3.5 & time < 6);
TFRdata{1}.mseavg.dat = nanmean(TFRdata{1}.mseavg.dat(:,idx_chan,1,timeIdx,:),4);
TFRdata{2}.mseavg.dat = nanmean(TFRdata{2}.mseavg.dat(:,idx_chan,1,timeIdx,:),4);

num_chans = size(TFRdata{1}.mseavg.dat,2);
num_freqs = size(TFRdata{1}.mseavg.dat,3);
num_time = size(TFRdata{1}.mseavg.dat,4);

num_subj_lst = [numel(IDs(ageIdx{1})), numel(IDs(ageIdx{2}))];
num_cond = 4;
num_grp = 2;

datamat_lst = cell(num_grp); lv_evt_list = [];
indCount_cont = 1;
for indGroup = 1:num_grp
    indCount = 1;
    curTFRdata = TFRdata{indGroup}.mseavg.dat;
    for indCond = 1:num_cond
        for indID = 1:num_subj_lst(indGroup)
            datamat_lst{indGroup}(indCount,:) = reshape(squeeze(curTFRdata(indID,:,:,:,indCond)), [], 1);
            lv_evt_list(indCount_cont) = indCond;
            indCount = indCount+1;
            indCount_cont = indCount_cont+1;
        end
    end
end

%% set PLS options and run PLS

option = [];
option.method = 1; % [1] | 2 | 3 | 4 | 5 | 6
option.num_perm = 1000; %( single non-negative integer )
option.num_split = 0; %( single non-negative integer )
option.num_boot = 1000; % ( single non-negative integer )
option.cormode = 0; % [0] | 2 | 4 | 6
option.meancentering_type = 0;% [0] | 1 | 2 | 3
option.boot_type = 'strat'; %['strat'] | 'nonstrat'

result = pls_analysis(datamat_lst, num_subj_lst, num_cond, option);

%% rearrange into fieldtrip structure

lvdat = reshape(result.boot_result.compare_u(:,1), num_chans, num_freqs, num_time);
%udat = reshape(result.u, num_chans, num_freqs, num_time);

stat = [];
stat.prob = lvdat;
stat.dimord = 'chan_freq_time';
stat.clusters = [];
stat.clusters.prob = result.perm_result.sprob; % check for significance of LV
stat.mask = lvdat > 3 | lvdat < -3;
stat.cfg = option;
stat.freq = freq;
stat.time = time(timeIdx);

save(fullfile(pn.dataInOUT, 'i01_taskPLSStim_YA_OA.mat'), 'stat', 'result', 'lvdat', 'lv_evt_list')
