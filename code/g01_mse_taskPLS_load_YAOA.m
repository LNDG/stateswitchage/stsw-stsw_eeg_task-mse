% Set up EEG PLS for a task PLS using multiscale entropy

clear all; cla; clc;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
rootpath = pwd;

pn.data         = fullfile(rootpath, 'data');
pn.dataInOUT    = fullfile(pn.data, 'mmse_output');
pn.tools        = fullfile(rootpath, 'tools');
    addpath(genpath(fullfile(pn.tools, '[MEG]PLS', 'MEGPLS_PIPELINE_v2.02b')))
    addpath(fullfile(pn.tools, 'fieldtrip')); ft_defaults;
    
%% add seed for reproducibility

rng(0, 'twister');

%% load data

filename = fullfile(rootpath, 'code', 'id_list.txt');
fileID = fopen(filename);
IDs = textscan(fileID,'%s');
fclose(fileID);
IDs = IDs{1};

ageIdx{1} = cellfun(@str2num, IDs, 'un', 1)<2000;
ageIdx{2} = cellfun(@str2num, IDs, 'un', 1)>2000;

load(fullfile(pn.dataInOUT, 'mseavg.mat'), 'mseavg');

% restrict to age group
for indGroup = 1:2
    TFRdata{indGroup}.mseavg = mseavg;
    TFRdata{indGroup}.mseavg.r = mseavg.r(ageIdx{indGroup},:,:,:,:);
    TFRdata{indGroup}.mseavg.dat = mseavg.dat(ageIdx{indGroup},:,:,:,:);
    TFRdata{indGroup}.mseavg.IDs = mseavg.IDs(ageIdx{indGroup});
end

%mseavg.dat % sub*chan*scale*time*cond

% load example data
tmp_data = load(fullfile(pn.data, 'mmse_input','1117_dim1_MSE_IN.mat'));

time = TFRdata{1}.mseavg.time;
freq = TFRdata{1}.mseavg.freq;
elec = tmp_data.data.elec;

% create datamat

timeIdx = find(time > 3.5 & time < 6);
TFRdata{1}.mseavg.dat = TFRdata{1}.mseavg.dat(:,:,:,timeIdx,:);
TFRdata{2}.mseavg.dat = TFRdata{2}.mseavg.dat(:,:,:,timeIdx,:);

num_chans = size(TFRdata{1}.mseavg.dat,2);
num_freqs = size(TFRdata{1}.mseavg.dat,3);
num_time = size(TFRdata{1}.mseavg.dat,4);

num_subj_lst = [numel(IDs(ageIdx{1})), numel(IDs(ageIdx{2}))];
num_cond = 4;
num_grp = 2;

datamat_lst = cell(num_grp); lv_evt_list = [];
indCount_cont = 1;
for indGroup = 1:num_grp
    indCount = 1;
    curTFRdata = TFRdata{indGroup}.mseavg.dat;
    for indCond = 1:num_cond
        for indID = 1:num_subj_lst(indGroup)
            datamat_lst{indGroup}(indCount,:) = reshape(squeeze(curTFRdata(indID,:,:,:,indCond)), [], 1);
            lv_evt_list(indCount_cont) = indCond;
            indCount = indCount+1;
            indCount_cont = indCount_cont+1;
        end
    end
end
datamat_lst{indGroup}(isnan(datamat_lst{indGroup})) = 0;

%% set PLS options and run PLS

option = [];
option.method = 1; % [1] | 2 | 3 | 4 | 5 | 6
option.num_perm = 1000; %( single non-negative integer )
option.num_split = 0; %( single non-negative integer )
option.num_boot = 1000; % ( single non-negative integer )
option.cormode = 0; % [0] | 2 | 4 | 6
option.meancentering_type = 0;% [0] | 1 | 2 | 3
option.boot_type = 'strat'; %['strat'] | 'nonstrat'

result = pls_analysis(datamat_lst, num_subj_lst, num_cond, option);

%% rearrange into fieldtrip structure

lvdat = reshape(result.boot_result.compare_u(:,1), num_chans, num_freqs, num_time);
%udat = reshape(result.u, num_chans, num_freqs, num_time);

stat = [];
stat.prob = lvdat;
stat.dimord = 'chan_freq_time';
stat.clusters = [];
stat.clusters.prob = result.perm_result.sprob; % check for significance of LV
stat.mask = lvdat > 3 | lvdat < -3;
stat.cfg = option;
stat.freq = freq;
stat.time = time(timeIdx);

save(fullfile(pn.dataInOUT, 'tmp_taskPLSStim_YAOA.mat'), 'stat', 'result', 'lvdat', 'lv_evt_list')
