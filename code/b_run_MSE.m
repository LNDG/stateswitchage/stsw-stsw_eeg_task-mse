function b_run_MSE(rootpath, id, cond)

if ismac % run if function is not pre-compiled
    id = '1126'; % test for example subject
    cond = 'dim1';
    currentFile = mfilename('fullpath');
    [pathstr,~,~] = fileparts(currentFile);
    cd(fullfile(pathstr,'..'))
    rootpath = pwd;
end

pn.dataIn      = fullfile(rootpath, 'data', 'mmse_input');
pn.dataOut  = fullfile(rootpath, 'data', 'mmse_output');
    if ~exist(pn.dataOut); mkdir(pn.dataOut); end
pn.tools	= fullfile(rootpath, 'tools'); addpath(pn.tools);
addpath(fullfile(pn.tools, 'fieldtrip')); ft_defaults;
addpath(fullfile(pn.tools, 'mMSE'))

%% select data

cfg_job.ID = id;
cfg_job.cond = cond;

load(fullfile(pn.dataIn, sprintf('%s_%s_MSE_IN.mat', cfg_job.ID, cfg_job.cond)))

%% setup mMSE

% The function expects fieldtrip-style input:
% data.{trial{},elec,time{},label{},fsample,sampleInfo,trialinfo}

% within each trial the sorting is channel*trial

cfg_entropy = [];
cfg_entropy.toi                 = -1.25:0.15:7.25;
cfg_entropy.timwin              = 0.5;
cfg_entropy.timescales          = unique(ceil(10.^[0:.1:2]));
cfg_entropy.freqRequest         = (1./cfg_entropy.timescales).*250;
cfg_entropy.coarsegrainmethod   = 'filtskip';
cfg_entropy.filtmethod          = 'lp';
cfg_entropy.m                   = 2; % pattern length
cfg_entropy.r                   = 0.5; % similarity criterion
cfg_entropy.recompute_r         = 'perscale_toi_sp';
cfg_entropy.polyremoval         = 2;
cfg_entropy.mem_available       = 8e9;
cfg_entropy.allowgpu            = 1;

mse = ft_entropyanalysis(cfg_entropy, data);

%% save output

save(fullfile(pn.dataOut, sprintf('%s_%s_MSE_OUT.mat', cfg_job.ID, cfg_job.cond)), 'mse', 'cfg_entropy', 'cfg_job')

end % function end