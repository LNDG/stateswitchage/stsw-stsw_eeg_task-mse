% Plot results from MSE analysis

% 180129 | JQK adapted function for STSWD study YA

% Note: For study data, channel positions were already rearranged during
% preprocessing.

%% set up paths

clear all; clc; restoredefaultpath;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
rootpath = pwd;

pn.dataInOUT    = fullfile(rootpath, 'data', 'mmse_output');
pn.figures      = fullfile(rootpath, 'figures');
pn.tools        = fullfile(rootpath, 'tools');
    addpath(fullfile(pn.tools));
    addpath(fullfile(pn.tools, 'fieldtrip')); ft_defaults;
    addpath(fullfile(pn.tools, 'BrewerMap'));
    addpath(fullfile(pn.tools, 'mseb'));

%% load data

BLlims = [2.5 2.75]; % within-condition mean baseline

load(fullfile(pn.dataInOUT, 'mseavg.mat'), 'mseavg');
load(fullfile(rootpath, 'data', 'elec.mat'));
mseavg.datBL=10*log10(mseavg.dat./repmat(nanmean(mseavg.dat(:,:,:,mseavg.time>=BLlims(1) & mseavg.time<BLlims(2),:),4), 1,1,1,numel(mseavg.time),1));
mseavg.rBL=10*log10(mseavg.r./repmat(nanmean(mseavg.r(:,:,:,mseavg.time>=BLlims(1) & mseavg.time<BLlims(2),:),4), 1,1,1,numel(mseavg.time),1));

%% raw: fast-scale occipital entropy increases with load

scales = 1;
channels = 58:60;

h = figure;
subplot(2,1,1);
    colorVec = brewermap(4, 'Dark2');
    grandAverage = squeeze(nanmean(nanmean(nanmean(mseavg.dat(:,channels,scales,:,1:4),5),3),2));
    for indCond = 1:4
        curData = squeeze(nanmean(nanmean(mseavg.dat(:,channels,scales,:,indCond),3),2));
        curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        lineProps.col = {colorVec(indCond,:)};
        l{indCond} = mseb(mseavg.time, nanmean(curData,1), standError, lineProps, 1); hold on;
    end
    legend([l{1}.mainLine, l{2}.mainLine, l{3}.mainLine, l{4}.mainLine], ...
        {'dim1'; 'dim2'; 'dim3'; 'dim4'}, 'orientation', 'horizontal', ...
        'location', 'South'); legend('boxoff');    xticks(mseavg.time(1:10:end))
    title('MSEn during StateSwitch');
    xlim([-1.25,6.25]); %ylim([-1 1]);
    set(findall(gcf,'-property','FontSize'),'FontSize',16);
    xlabel('Time [s]'); ylabel('MSEn [a.u.]')
subplot(2,1,2);
    colorVec = brewermap(4, 'Dark2');
    grandAverage = squeeze(nanmean(nanmean(nanmean(mseavg.r(:,channels,scales,:,1:4),5),3),2));
    for indCond = 1:4
        curData = squeeze(nanmean(nanmean(mseavg.r(:,channels,scales,:,indCond),3),2));
        curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        lineProps.col = {colorVec(indCond,:)};
        l{indCond} = mseb(mseavg.time, nanmean(curData,1), standError, lineProps, 1); hold on;
    end
    xticks(mseavg.time(1:10:end))
    title('R param during StateSwitch');
    xlim([-1.25,6.5]); %ylim([-.3 .3]);
    set(findall(gcf,'-property','FontSize'),'FontSize',16);
    xlabel('Time [s]'); ylabel('R param [a.u.]')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

suptitle(['scales = ', num2str(scales), ', channels = ', num2str(channels)]);

figureName = 'd_FastScaleTime';
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

%% baseline corrected: fast-scale occipital entropy increases with load

scales = 1;
channels = 58:60;

h = figure;
subplot(2,1,1);
%     patches.timeVec = [-1.500 0 1.000 3.000 6.000 8.000 9.500];
%     patches.colorVec = [1 1 1;.9 .9 .9; 1 1 1; 1 .9 .9; 1 1 1; .9 .9 .9];
%     for indP = 1:numel(patches.timeVec)-1
%         p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
%                     [-1 -1 1*[1 1]], patches.colorVec(indP,:));
%         p.EdgeColor = 'none';
%     end; hold on;
    colorVec = brewermap(4, 'Dark2');
    grandAverage = squeeze(nanmean(nanmean(nanmean(mseavg.datBL(:,channels,scales,:,1:4),5),3),2));
    for indCond = 1:4
        curData = squeeze(nanmean(nanmean(mseavg.datBL(:,channels,scales,:,indCond),3),2));
        curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        lineProps.col = {colorVec(indCond,:)};
        l{indCond} = mseb(mseavg.time, nanmean(curData,1), standError, lineProps, 1); hold on;
    end
    legend([l{1}.mainLine, l{2}.mainLine, l{3}.mainLine, l{4}.mainLine], ...
        {'dim1'; 'dim2'; 'dim3'; 'dim4'}, 'orientation', 'horizontal', ...
        'location', 'South'); legend('boxoff');    xticks(mseavg.time(1:10:end))
    title('MSEn during StateSwitch');
    xlim([-1.25,6.25]); %ylim([-1 1]);
    set(findall(gcf,'-property','FontSize'),'FontSize',16);
    xlabel('Time [s]'); ylabel('MSEn [a.u.]')
subplot(2,1,2);
    colorVec = brewermap(4, 'Dark2');
    grandAverage = squeeze(nanmean(nanmean(nanmean(mseavg.rBL(:,channels,scales,:,1:4),5),3),2));
    for indCond = 1:4
        curData = squeeze(nanmean(nanmean(mseavg.rBL(:,channels,scales,:,indCond),3),2));
        curData = curData-grandAverage+repmat(nanmean(grandAverage,1),size(grandAverage,1),1);
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        lineProps.col = {colorVec(indCond,:)};
        l{indCond} = mseb(mseavg.time, nanmean(curData,1), standError, lineProps, 1); hold on;
    end
    xticks(mseavg.time(1:10:end))
    title('R param during StateSwitch');
    xlim([-1.25,6.5]); %ylim([-.3 .3]);
    set(findall(gcf,'-property','FontSize'),'FontSize',16);
    xlabel('Time [s]'); ylabel('R param [a.u.]')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

suptitle(['scales = ', num2str(scales), ', channels = ', num2str(channels)]);

figureName = 'd_FastScaleTime_bl';
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

%% topography of baseline corrected fast scales

idx_time = mseavg.time>3.5 & mseavg.time<6;
idx_scale = 1;

cfg = [];
cfg.layout = 'acticap-64ch-standard2.mat';
cfg.parameter = 'powspctrm';
cfg.comment = 'no';
cfg.colorbar = 'SouthOutside';
%cfg.zlim = [-3 3];

h = figure('units','normalized','position',[.1 .1 .4 .7]);
subplot(2,2,1);
    cfg.figure = h;
    to_plot = squeeze(nanmean(nanmean(nanmean(nanmean(mseavg.datBL(:,:,idx_scale,idx_time,1:4),5),4),3),1));
    plotData = [];
    plotData.label = elec.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = to_plot';
    ft_topoplotER(cfg,plotData);
    title('Fast-scale MSE, 1-4 Target')
subplot(2,2,2);
    cfg.figure = h;
    to_plot = squeeze(nanmean(nanmean(nanmean(nanmean(mseavg.rBL(:,:,idx_scale,idx_time,1:4),5),4),3),1));
    plotData = [];
    plotData.label = elec.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = to_plot';
    ft_topoplotER(cfg,plotData);
    title('Fast-scale R, 1-4 Target')
subplot(2,2,3);
    cfg.figure = h;
    l1 = squeeze(nanmean(nanmean(nanmean(nanmean(mseavg.datBL(:,:,idx_scale,idx_time,1),5),4),3),1));
    l2 = squeeze(nanmean(nanmean(nanmean(nanmean(mseavg.datBL(:,:,idx_scale,idx_time,2),5),4),3),1));
    l3 = squeeze(nanmean(nanmean(nanmean(nanmean(mseavg.datBL(:,:,idx_scale,idx_time,3),5),4),3),1));
    l4 = squeeze(nanmean(nanmean(nanmean(nanmean(mseavg.datBL(:,:,idx_scale,idx_time,4),5),4),3),1));
    plotData = [];
    plotData.label = elec.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = (nanmean([l2;l3;l4],1)-l1)';
    ft_topoplotER(cfg,plotData);
    title('Fast-scale MSE, 234-1 Target')
subplot(2,2,4);
    cfg.figure = h;
    l1 = squeeze(nanmean(nanmean(nanmean(nanmean(mseavg.rBL(:,:,idx_scale,idx_time,1),5),4),3),1));
    l2 = squeeze(nanmean(nanmean(nanmean(nanmean(mseavg.rBL(:,:,idx_scale,idx_time,2),5),4),3),1));
    l3 = squeeze(nanmean(nanmean(nanmean(nanmean(mseavg.rBL(:,:,idx_scale,idx_time,3),5),4),3),1));
    l4 = squeeze(nanmean(nanmean(nanmean(nanmean(mseavg.rBL(:,:,idx_scale,idx_time,4),5),4),3),1));
    plotData = [];
    plotData.label = elec.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = (nanmean([l2;l3;l4],1)-l1)';
    ft_topoplotER(cfg,plotData);
    title('Fast-scale R, 234-1 Target')
set(findall(gcf,'-property','FontSize'),'FontSize',18)
figureName = 'd_FastScaleTopo';
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');
