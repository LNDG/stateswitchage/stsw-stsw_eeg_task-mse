function a2_calculateSpectra(id, rootpath)

% Calculate 1/f spectra formMSE inputs (alha should be removed)

if ismac % run if function is not pre-compiled
    id = '1126'; % test for example subject
    currentFile = mfilename('fullpath');
    [pathstr,~,~] = fileparts(currentFile);
    cd(fullfile(pathstr,'..'))
    rootpath = pwd;
end

pn.dataIn	= fullfile(rootpath, '..', 'X1_preprocEEGData');
pn.data     = fullfile(rootpath, 'data', 'mmse_input');
pn.dataOut  = fullfile(rootpath, 'data', 'mmse_freq');
	if ~exist(pn.dataOut); mkdir(pn.dataOut); end
pn.tools    = fullfile(rootpath, 'tools');
    addpath(fullfile(pn.tools, 'fieldtrip')); ft_defaults;

display(['processing ID ' id]);

for indCond = 1:4

    load(fullfile(pn.data, sprintf('%s_%s_MSE_IN.mat', id, ['dim',num2str(indCond)])),'data'),

    param.FFT.trial       = 'all';
    param.FFT.method      = 'mtmconvol';    % analyze entire spectrum; multitaper
    param.FFT.output      = 'pow';          % only get power values
    param.FFT.keeptrials  = 'no';           % return individual trials
    param.FFT.taper       = 'hanning';      % use hanning windows
    param.FFT.tapsmofrq   = 2;
    param.FFT.foi         = 2.^[0:.125:6.5];  % frequencies of interest
    param.FFT.t_ftimwin   = repmat(9,1,numel(param.FFT.foi));
    param.FFT.toi         = 4.5;
    param.FFT.pad         = 20;

    tmpFreq = ft_freqanalysis(param.FFT, data);

%         figure; plot(param.FFT.foi, log10(squeeze(nanmean(tmpFreq.powspctrm(55:60,:),1))))

    if indCond == 1
        freq = tmpFreq;
        freq.powspctrm = [];
        freq.powspctrm(1,:,:) = tmpFreq.powspctrm;
    else
        freq.powspctrm(indCond,:,:) = tmpFreq.powspctrm;
    end
    clear tmpFreq;

end

%% save output

save(fullfile(pn.dataOut, [id, '_freq.mat']), 'freq');
