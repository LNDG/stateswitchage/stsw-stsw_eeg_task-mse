function a_create_mMSE_input(id, rootpath)

% Function creates input data to MSE, segmented by dimensionality condition

if ismac % run if function is not pre-compiled
    id = '1126'; % test for example subject
    currentFile = mfilename('fullpath');
    [pathstr,~,~] = fileparts(currentFile);
    cd(fullfile(pathstr,'..'))
    rootpath = pwd;
end

pn.dataIn	= fullfile(rootpath, '..', 'X1_preprocEEGData');
pn.data     = fullfile(rootpath, 'data');
pn.dataOut  = fullfile(rootpath, 'data', 'mmse_input');
    if ~exist(pn.dataOut); mkdir(pn.dataOut); end
pn.tools	= fullfile(rootpath, 'tools'); addpath(pn.tools);
addpath(fullfile(pn.tools, 'fieldtrip')); ft_defaults;

display(['processing ID ', id]);

%% preproc the data

cond_names = {'dim1'; 'dim2'; 'dim3'; 'dim4'};

% load dataset
fprintf('Loading Subject %s: . . .\n', id(1:4))
datfile = fullfile(pn.dataIn, [id, '_dynamic_EEG_Rlm_Fhl_rdSeg_Art_Cond.mat']);
load(datfile, 'data');
data_orig = data; clear data;
for indCond = 1:4
    % select data from the condition of interest and save condition data
    cfg = []; 
    cfg.trials = find(nansum(data_orig.TrlInfo(:,1:4),2)==indCond);
    [dataCond] = ft_selectdata(cfg, data_orig);
    dataCond.fsample = 500;
    TrlInfo = dataCond.TrlInfo;
    TrlInfoLabels = dataCond.TrlInfoLabels;
    data=dataCond; clear dataCond;
    % CSD transform
    csd_cfg = [];
    csd_cfg.elecfile = 'standard_1005.elc';
    csd_cfg.method = 'spline';
    % add .elec field
    load(fullfile(pn.data, 'elec.mat'))
    data.elec = elec;
    data = ft_scalpcurrentdensity(csd_cfg, data);
    data.TrlInfo = TrlInfo; clear TrlInfo;
    data.TrlInfoLabels = TrlInfoLabels; clear TrlInfoLabels;
    data.cfg = data_orig.cfg; clear cfg_old;
    data.cfg.csd = csd_cfg;
    % notch-filter the 7-16 Hz alpha band
    cfg_hpf.bsfilter = 'yes';
    cfg_hpf.bsfreq = [7 16];
    cfg_hpf.bsfiltord = 6;
    [data] = ft_preprocessing(cfg_hpf, data);
    save(fullfile(pn.dataOut, sprintf('%s_%s_MSE_IN.mat', id(1:4), cond_names{indCond})),'data');
end
