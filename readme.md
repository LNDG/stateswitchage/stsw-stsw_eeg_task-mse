[![made-with-datalad](https://www.datalad.org/badges/made_with.svg)](https://datalad.org)

## Time-resolved sample entropy

Sample entropy 34 quantifies the irregularity of a time series of length N by assessing the conditional probability that two sequences of m consecutive data points will remain similar when another sample (m+1) is included in the sequence (for a visual example see Figure 1A in 17).  Sample entropy is defined as the inverse natural logarithm of this conditional similarity: `SampEn(m,r,N)=-log⁡((p^(m+1) (r))/(p^m (r) ))`. The similarity criterion (r) defines the tolerance within which two points are considered similar and is defined relative to the standard deviation (~variance) of the signal (here set to r = .5). We set the sequence length m to 2, in line with previous applications 17. An adapted version of sample entropy calculations implemented in the mMSE toolbox (available from `https://github.com/LNDG/mMSE`) was used 17,91,92, wherein entropy is estimated across discontinuous data segments to provide time-resolved estimates. The estimation of scale-wise entropy across trials allows for an estimation of coarse scale entropy also for short time-bins (i.e., without requiring long, continuous signals), while quickly converging with entropy estimates from continuous recordings 91. To remove the influence of posterior-occipital low-frequency rhythms on entropy estimates, we notch-filtered the 7-16 Hz alpha band using 6th order Butterworth filter prior to the entropy calculation 17. Time-resolved entropy estimates were calculated for 500 ms windows from -1 s pre-stimulus to 1.25 s post-probe with a step size of 150 ms. As entropy values are implicitly normalized by the variance in each time bin via the similarity criterion, no temporal baselining was required. Linear load effects on entropy were assessed by univariate cluster-based permutation tests on channel x time data (see Univariate statistical analyses using cluster-based permutation tests).

**a_create_mMSE_input**

- select conditions
- CSD transform
- notch-filter 7-16 Hz alpha band (6th order Buttwerworth)

**ax01_calculateSpectra**

- check spectra of input: ensure that no alpha peak exists
- 1 Hanning multitaper spectrum calculated across -3 s prestim to +3 s poststim
- 20 s padding

**ax02_plotSpectra**

- plot spectrum across O1, Oz and O2, avg. across all subjects and conditions

![figure](figures/a3_input_spectrum.png)

**b_run_MSE**

- run modified multiscale entropy (see description above)

```
cfg_entropy = [];
cfg_entropy.toi                 = -1.25:0.15:7.25;
cfg_entropy.timwin              = 0.5;
cfg_entropy.timescales          = unique(ceil(10.^[0:.1:2]));
cfg_entropy.freqRequest         = (1./cfg_entropy.timescales).*250;
cfg_entropy.coarsegrainmethod   = 'filtskip';
cfg_entropy.filtmethod          = 'lp';
cfg_entropy.m                   = 2; % pattern length
cfg_entropy.r                   = 0.5; % similarity criterion
cfg_entropy.recompute_r         = 'perscale_toi_sp';
cfg_entropy.polyremoval         = 2;
cfg_entropy.mem_available       = 8e9;
cfg_entropy.allowgpu            = 1;
```

**c_mse_merge**

- collect individual mMSE outputs in a joint structure (YA and OA combined here)

**d_mse_plot**

- plot summary traces and topographies for raw and baselined data
- currently not reported

Raw traces at occipital channels for scale 1:
![figure](figures/d_FastScaleTime.png)

**e01_linearTest**

- CBPA of linear effect during stimulus presentation
- plot topography and temporal traces

!!! warning "extracted data are from occipital channels O1, Oz, O2"
    For the initial YA paper, we extracted data across all channels in the YA cluster. To harmonize, we focus on the occipital channels here.

Younger adults:
![figure](figures/e01_EntropyTopo_ya.png)
![figure](figures/e01_EntropyTraces_ya.png)
![figure](figures/e01_entropy_modulation_RCplot_ya.png)

Older adults:
![figure](figures/e01_EntropyTopo_oa.png)
![figure](figures/e01_EntropyTraces_oa.png)
![figure](figures/e01_entropy_modulation_RCplot_oa.png)

**e02_plot_traces**

- plot temporal traces from channel Oz

Younger adults:
![figure](figures/e02_EntropyTraces_ya.png)

Older adults:
![figure](figures/e02_EntropyTraces_oa.png)

Combined:
![figure](figures/e02_EntropyTraces_yaoa.png)

**g01_taskPLS_load_YAOA**

- 2-group task pls model including prestim and probe periods, multiscale entropy signals
- lv1 shows strong loadings during probe period, early scale increases over posterior channels, medium decreases over motor channels, slow scale increases over motor
- lv2 is similar to the sample entropy increases (also observed for younger, but not older adults)

**k01_taskPLS_load_YAOA**

- 2-group task pls model including sample entropy during the final 2.5s of stimulus presentation and 1/f slopes (estimated across the same period) only

LV1 shows non-linear decreases in irregularity over frontal channels:
![figure](figures/k01_pls_topo.png)
![figure](figures/k01_pls_rcp.png)

LV1 shows irregularity increases over posterior channels (YA only):
![figure](figures/k02_pls_topo.png)
![figure](figures/k02_pls_rcp.png)

**h01_sampen_taskPLS_load_YAOA**

- 2-group task pls model including sample entropy averaged across the final 2.5s of stimulus presentation
- only posterior-occipital channels are included in model

![figure](figures/h01_pls_topo.png)
![figure](figures/h01_pls_rcp.png)